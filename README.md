

## Восстановление базы данных MySQL из последнего бэкапа на AWS S3

Этот Jenkins Pipeline скрипт предназначен для поиска последнего дампа MySQL в указанном S3 bucket на AWS и его восстановления в текущую базу данных. Скрипт выполняет следующие шаги:

1. **Поиск последнего бэкапа**: Сценарий ищет последний файл бэкапа в заданном S3 bucket, сортируя их по времени создания.
2. **Скачивание последнего бэкапа**: Скачивает найденный файл бэкапа в локальную директорию Jenkins агента.
3. **Восстановление базы данных**: Восстанавливает текущую базу данных MySQL с использованием скачанного файла бэкапа.
4. **Очистка**: Удаляет локально скачанный файл после завершения восстановления.

### Настройка

Перед использованием этого скрипта, убедитесь, что:

- Jenkins установлен и настроен.
- Установлены необходимые плагины (например, Pipeline).
- На Jenkins агенте установлен и настроен AWS CLI.
- В Jenkins настроены учетные данные для доступа к базе данных MySQL и S3 bucket.

### Pipeline скрипт

Добавьте следующий скрипт в ваш Pipeline job в Jenkins:

```groovy
pipeline {
    agent any

    environment {
        DB_HOST = 'localhost'
        DB_USER = 'your_username'
        DB_PASS = 'your_password'
        DB_NAME = 'your_database'
        S3_BUCKET = 's3://your-bucket-name'
        LOCAL_RESTORE_DIR = '/tmp/mysql_restore'
    }

    stages {
        stage('Find Latest Backup') {
            steps {
                script {
                    // Ensure the local restore directory exists
                    sh "mkdir -p ${env.LOCAL_RESTORE_DIR}"

                    // Find the latest backup file in the S3 bucket
                    def latest_backup = sh(script: "aws s3 ls ${env.S3_BUCKET}/ --recursive | sort | tail -n 1 | awk '{print \$4}'", returnStdout: true).trim()

                    if (latest_backup) {
                        env.LATEST_BACKUP_FILE = "${env.S3_BUCKET}/${latest_backup}"
                        echo "Latest backup file found: ${env.LATEST_BACKUP_FILE}"
                    } else {
                        error "No backup files found in the S3 bucket."
                    }
                }
            }
        }

        stage('Download Latest Backup') {
            steps {
                sh "aws s3 cp ${env.LATEST_BACKUP_FILE} ${env.LOCAL_RESTORE_DIR}/latest_backup.sql"
            }
        }

        stage('Restore Database') {
            steps {
                sh """
                mysql -h ${DB_HOST} -u ${DB_USER} -p${DB_PASS} ${DB_NAME} < ${env.LOCAL_RESTORE_DIR}/latest_backup.sql
                """
            }
        }
    }

    post {
        cleanup {
            // Clean up the local restore directory
            sh "rm -rf ${env.LOCAL_RESTORE_DIR}"
        }
    }
}
```

### Переменные окружения

- `DB_HOST`: Хост базы данных MySQL.
- `DB_USER`: Имя пользователя базы данных MySQL.
- `DB_PASS`: Пароль пользователя базы данных MySQL.
- `DB_NAME`: Имя базы данных MySQL.
- `S3_BUCKET`: Путь к S3 bucket, где хранятся бэкапы.
- `LOCAL_RESTORE_DIR`: Локальная директория на Jenkins агенте для хранения скачанных бэкапов.

### Крон расписание

Добавьте триггер для регулярного запуска джоба, например, для ежедневного запуска в 02:00, используйте следующее расписание в разделе Triggers:

```groovy
triggers {
    cron('0 2 * * *')
}
```

### Примечание

Убедитесь, что у вас есть необходимые права доступа для чтения файлов из S3 bucket и записи в базу данных MySQL.